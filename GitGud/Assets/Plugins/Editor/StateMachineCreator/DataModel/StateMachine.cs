﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class StateMachine
{
    public string name = null;
    public List<State> states = new List<State>();

    public StateMachine(string name, List<State> states)
    {
        this.name = name;
        this.states = states;
    }

    public StateMachine()
    {

    }
}
