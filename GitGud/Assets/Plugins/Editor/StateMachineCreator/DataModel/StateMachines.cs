﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class StateMachines {
    public List<StateMachine> myStateMachines = new List<StateMachine>();

    public StateMachines(List<StateMachine> myStateMachines)
    {
        this.myStateMachines = myStateMachines;
    }

    public StateMachines()
    {

    }
}
