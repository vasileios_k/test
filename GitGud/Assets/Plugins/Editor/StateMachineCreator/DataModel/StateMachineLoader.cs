﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public static class StateMachineLoader
{

    public const string FILENAME = "StateMachines.json";
    public static List<StateMachine> FromJson(string path)
    {
        path = path.Replace("Templates", "DataModel") + FILENAME;


        if (File.Exists(path))
        {
            //Loads the text of the JSON file from the resources folder.
            string json = File.ReadAllText(path);


            //Converts the JSON text into an objects array and returns it.
            return JsonUtility.FromJson<StateMachines>(json).myStateMachines;
        }
        else
        {
            return new List<StateMachine>() ;
        }
    }

    public static void CreateJson(StateMachines stateMachines, string path)
    {
        string json = JsonUtility.ToJson(stateMachines, true);
        File.WriteAllText(path.Replace("Templates", "DataModel") + FILENAME, json);
    }
}
