﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class State
{
    public string name = null;
    public bool isStart = false;
    public List<string> transitionsTo = new List<string>();

    public State(string name, bool isStart, List<string> transitionsTo)
    {
        this.name = name;
        this.isStart = isStart;
        this.transitionsTo = transitionsTo;
    }

    public State()
    {

    }
}
