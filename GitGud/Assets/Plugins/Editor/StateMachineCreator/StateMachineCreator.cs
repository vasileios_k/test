﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.IO;

public class StateMachineCreator : EditorWindow
{
    StateMachine sm = new StateMachine();
    public enum en { UP, DOWN, LEFT };
    string SMName = "";
    public string[] SMStates = { };
    string[] tempSMStates = { };
    string StateMachinePath;
    string TemplatesPath;
    int selectedInitial = 0;
    Dictionary<string, Dictionary<string, bool>> myCheckboxesTo = new Dictionary<string, Dictionary<string, bool>>();
    bool dicsReady = false;
    private List<string> tempSMList;

    private void Awake()
    {
        InitializePaths();
    }


    void InitializePaths()
    {
        StateMachinePath = Application.dataPath + "\\Scripts\\StateMachines\\";
        string pluginPath = Directory.GetParent(AssetDatabase.GetAssetPath(MonoScript.FromScriptableObject(this))).FullName;
        TemplatesPath = pluginPath + "\\Templates\\";

    }

    [MenuItem("State Machine/Creator")]

    public static void ShowWindow()
    {
        GetWindow(typeof(StateMachineCreator), false, "SM Creator");
    }

    void OnGUI()
    {
        if (!dicsReady)
        {
            GUILayout.Label("State Machine Properties", EditorStyles.boldLabel);
            SMName = EditorGUILayout.TextField("State Machine Name", SMName);

            ScriptableObject target = this;
            SerializedObject so = new SerializedObject(target);
            SerializedProperty stringsProperty = so.FindProperty("SMStates");
            EditorGUILayout.PropertyField(stringsProperty, new GUIContent("State Names"), true); // True means show children
            so.ApplyModifiedProperties(); // Remember to apply modified properties

            tempSMStates = new string[SMStates.Length + 1];
            tempSMStates[0] = "NONE";
            SMStates.CopyTo(tempSMStates, 1);

            selectedInitial = EditorGUILayout.Popup("Initial State", selectedInitial, tempSMStates);


            AddTransitionsButtonClick(tempSMStates);
        }
        else
        {
            EditorGUILayout.BeginVertical();
            {
                GUILayout.Label(SMName + " State Machine", EditorStyles.boldLabel);

                CreateTransitionsUI(tempSMList);

            }
            EditorGUILayout.EndVertical();

            CreateButtonClick();
        }
    }

    void CreateButtonClick()
    {

        if (GUILayout.Button("Build State Machine"))
        {

            foreach (State state in sm.states)
            {
                foreach (State st in sm.states)
                {
                    if (state != st)
                    {
                        if (myCheckboxesTo[state.name][st.name])
                        {
                            state.transitionsTo.Add(st.name);
                        }
                    }
                }
            }
            StateMachines sms = new StateMachines(StateMachineLoader.FromJson(TemplatesPath));
            sms.myStateMachines.Add(sm);
            StateMachineLoader.CreateJson(sms, TemplatesPath);

            StateMachinePath += SMName + "StateMachine\\";

            if (Directory.Exists(StateMachinePath))
            {
                Directory.Delete(StateMachinePath, true);
            }
            Directory.CreateDirectory(StateMachinePath);

            CreateAnyStateFile();
            CreateHandlerFile();
            foreach (string state in SMStates)
            {
                CreateStateFile(state);
            }
            CreateInstallerFile();
            CreateSettingsInstallerFile();

            AssetDatabase.Refresh();
            Debug.Log("State Machine with name " + SMName + " created successfully.");
            Restart();

        }
    }

    void Restart()
    {
        dicsReady = false;
        sm = new StateMachine();
        SMName = "";
        selectedInitial = 0;
        myCheckboxesTo = new Dictionary<string, Dictionary<string, bool>>();
        dicsReady = false;
        tempSMList = new List<string>();
        InitializePaths();
    }
    void AddTransitionsButtonClick(string[] smArray)
    {
        if (GUILayout.Button("Add Transitions"))
        {

            if (!FindDuplicateStates())
            {
                if (SMName.Length > 0 && SMStates.Length > 0 && IsValidInitialState())
                {
                    tempSMList = new List<string>(smArray);
                    tempSMList.RemoveAt(0);
                    dicsReady = true;
                    InitializeDictionaries(tempSMList);

                    sm = new StateMachine();
                    sm.name = SMName;
                    for (int i = 0; i < tempSMList.Count; i++)
                    {
                        State st = new State();
                        st.name = tempSMList[i];
                        if (i == selectedInitial - 1)
                        {
                            st.isStart = true;
                        }
                        else
                        {
                            st.isStart = false;
                        }
                        sm.states.Add(st);

                    }
                }
                else
                {
                    Debug.LogError("Invalid input field(s). Check again and retry");
                }
            }
            else
            {
                PrintDuplicateState();
            }

        }
    }

    void CreateTransitionsUI(List<string> transitions)
    {
        GUILayout.Label("Transitions To", EditorStyles.boldLabel);

        for (int i = 0; i < transitions.Count; i++)
        {
            CreateTransitionsForOneState(transitions, i);
        }


    }

    void CreateTransitionsForOneState(List<string> transitions, int transition)
    {
        EditorGUILayout.BeginHorizontal("Box", GUILayout.ExpandWidth(false), GUILayout.MinWidth(0));
        {
            GUILayout.Space(-5);
            EditorGUILayout.BeginVertical(GUILayout.ExpandWidth(false), GUILayout.MinWidth(0));
            {
                EditorGUILayout.Space();
                EditorGUILayout.LabelField(transitions[transition], EditorStyles.boldLabel, GUILayout.ExpandWidth(false), GUILayout.MinWidth(0));
            }
            EditorGUILayout.EndVertical();
            for (int i = 0; i < transitions.Count; i++)
            {
                if (i != transition)
                {
                    GUILayout.FlexibleSpace();

                    EditorGUILayout.BeginVertical(GUILayout.ExpandWidth(false), GUILayout.MinWidth(0));
                    {
                        EditorGUILayout.LabelField(transitions[i], GUILayout.ExpandWidth(false), GUILayout.MinWidth(0));
                        myCheckboxesTo[transitions[transition]][transitions[i]] = EditorGUILayout.Toggle(myCheckboxesTo[transitions[transition]][transitions[i]], GUILayout.ExpandWidth(false), GUILayout.MinWidth(0));

                    }
                    EditorGUILayout.EndVertical();
                }
            }
            GUILayout.FlexibleSpace();
        }
        EditorGUILayout.EndHorizontal();
        EditorGUILayout.Space();
    }

    void InitializeDictionaries(List<string> states)
    {
        myCheckboxesTo.Clear();
        foreach (string state in states)
        {
            myCheckboxesTo.Add(state, new Dictionary<string, bool>());

            foreach (string name in states)
            {
                if (state != name)
                {
                    myCheckboxesTo[state].Add(name, false);
                }
            }
        }
    }

    bool IsValidInitialState()
    {
        if (selectedInitial > 0)
            return true;
        else
            return false;
    }

    bool FindDuplicateStates()
    {
        List<string> list = new List<string>();
        foreach (string state in SMStates)
        {
            if (list.Contains(state.ToLower()))
            {
                return true;
            }
            else
                list.Add(state.ToLower());
        }

        return false;
    }

    void PrintDuplicateState()
    {
        List<string> list = new List<string>();
        foreach (string state in SMStates)
        {
            if (list.Contains(state.ToLower()))
            {
                Debug.LogError("Found duplicate state: " + state);
                return;
            }
            else
                list.Add(state.ToLower());
        }

    }

    private void CreateStateFile(string state)
    {
        string txt = File.ReadAllText(TemplatesPath + "TemplateState.txt");

        string transitions = "";
        foreach (State stateO in sm.states)
        {
            if (stateO.name != state) continue;
            foreach (string transition in stateO.transitionsTo)
            {
                transitions += "\r\n\tpublic override void To" + transition + "State(params Object[] arguments)";
                transitions += "\r\n\t{";
                transitions += "\r\n\t\tbase.To" + transition + "State(arguments);";
                transitions += "\r\n\t\t_manager.CurrentState = " + SMName + "States." + transition + ";";
                transitions += "\r\n\t}\r\n";
            }
            break;
        }


        txt = string.Format(txt, SMName, state, transitions);
        File.WriteAllText(StateMachinePath + SMName + "State" + state + ".cs", txt);
    }

    private void CreateAnyStateFile()
    {
        string txt = File.ReadAllText(TemplatesPath + "TemplateStateAny.txt");

        string states = "";
        foreach (string state in SMStates)
        {
            states += "\r\n\tpublic virtual void To" + state + "State(params Object[] arguments) { _argumentsSignal.Fire(arguments); }\r\n";
        }

        txt = string.Format(txt, SMName, states);
        File.WriteAllText(StateMachinePath + SMName + "StateAny.cs", txt);

    }

    private void CreateHandlerFile()
    {
        string txt = File.ReadAllText(TemplatesPath + "TemplateStateManager.txt");

        string publics = "";
        string initial = "";
        string states = "";
        string stateNames = "";
        foreach (string state in SMStates)
        {
            states += "\r\n\tvoid To" + state + "State(params Object[] arguments);";
            stateNames += "\r\n\t" + state + ",";
            publics += state + " " + state.ToLower() + ", ";
            initial += state.ToLower() + ", ";

        }
        stateNames += "\r\n\tNone";
        publics = publics.Remove(publics.Length - 2, 2);
        publics += ")";
        initial = initial.Remove(initial.Length - 2, 2);
        txt = string.Format(txt, SMName, states, publics, initial, SMStates[selectedInitial - 1], stateNames);
        File.WriteAllText(StateMachinePath + SMName + "StateMachine.cs", txt);
    }

    private void CreateInstallerFile()
    {
        string txt = File.ReadAllText(TemplatesPath + "TemplateInstaller.txt");

        string states = "";
        foreach (string state in SMStates)
        {
            states += "\r\n\t\tContainer.Bind<" + SMName + "StateMachine." + state + ">().AsSingle();";
        }
        string signal = "\r\n\t\tContainer.DeclareSignal<" + SMName + "StateMachine.ArgumentsSignal>();";
        signal += "\r\n\t\tContainer.BindSignal<Object[], " + SMName + "StateMachine.ArgumentsSignal>().To<" + SMName + "StateMachine>(x => x.SendStateArguments).AsSingle();";

        txt = string.Format(txt, SMName, states, signal);
        Directory.CreateDirectory(StateMachinePath + "Installers");
        File.WriteAllText(StateMachinePath + "Installers/" + SMName + "Installer.cs", txt);
    }

    private void CreateSettingsInstallerFile()
    {
        string txt = File.ReadAllText(TemplatesPath + "TemplateSettingsInstaller.txt");

        string states = "";
        string constructor = "";
        foreach (string state in SMStates)
        {
            states += "\r\n\tpublic " + SMName + "StateMachine." + state + ".Settings " + SMName + state + ";";
            constructor += "\r\n\t\tContainer.BindInstance(" + SMName + state + ");";

        }
        states += "\r\n\tpublic " + SMName + "Installer.Settings " + SMName + "Installer;";
        constructor += "\r\n\t\tContainer.BindInstance(" + SMName + "Installer);";

        txt = string.Format(txt, SMName, states, constructor);
        Directory.CreateDirectory(StateMachinePath + "Installers");
        File.WriteAllText(StateMachinePath + "Installers/" + SMName + "SettingsInstaller.cs", txt);
    }
}
